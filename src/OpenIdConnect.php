<?php

namespace Sketch;

use Psr\Container\ContainerInterface;
use Firebase\JWT\JWT;
use Firebase\JWT\JWK;

class OpenIdConnect
{
    protected $view;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->view = $container->get("view");
        $this->session = $container->get("session");
        $this->userFactory = $container->get("userFactory");
        $this->http = new \GuzzleHttp\Client();       
    }

    public function home($request, $response, $args) {
        if(!$this->session->exists('accessToken')) {
            return $response->withStatus(200)->withRedirect('/login/'. ZS_SAC_OPENID_ENVIRONMENT); 
        }
        return $response->withStatus(200)->withRedirect('/info');
    }

    public function loginDev($request, $response, $args) {
        $this->session->set('provider', 'dev');
        return $this->login($request, $response, $args, $this->container->get("providers")['dev']);
        
    }
    public function loginProd($request, $response, $args) {
        $this->session->set('provider', 'prod');
        return $this->login($request, $response, $args, $this->container->get("providers")['prod']);
    }

    public function login($request, $response, $args, $provider) {
        $discovery = $provider->discovery();
        
        $nonce = bin2hex(openssl_random_pseudo_bytes(20));          
        $state = (object) [
            'check' => bin2hex(openssl_random_pseudo_bytes(20)),
            'state' => 'login' 
        ]; 
        $this->session->set('state', $state);
        $this->session->set('nonce', $nonce);
        
        return $this->view->render($response, 'login.html', [
            'url' => $discovery->authorization_endpoint,
            'parameters' => [
                'client_id' => $provider->clientId(),
                'response_type' => 'code',
                'scope' => $provider->scope(), 
                'redirect_uri' => $provider->redirectUri(),
                'state' => $state->check,
                'nonce' => $nonce
            ]
        ]);
    }

    public function logout($request, $response, $args) {
        $provider = $this->getCurrentProvider();
        $discovery = $provider->discovery();
        $tokens = $this->session->get('tokens'); 

        $state = (object) [
            'check' => bin2hex(openssl_random_pseudo_bytes(20)),
            'state' => 'checksession' 
        ];
        $this->session->set('state', $state);

        $checkSessionParameters = [
            'client_id' => $provider->clientId(),
            'response_type' => 'code',
            'scope' => $provider->scope(), 
            'redirect_uri' => $provider->redirectUri(),
            'prompt' => 'none',
            'state' => $state->check
        ];
        $query = http_build_query($checkSessionParameters);
        return $response->withStatus(200)->withRedirect($discovery->authorization_endpoint . '?' . $query  );
    }

    public function info($request, $response, $args) {
        if(!$this->session->exists('tokens')) {
            return $response->withStatus(200)->withRedirect('/'); 
        }

        try {
            $userinfoResponse = $this->fetchUserInfo();
        } catch (\Exception $e) {
            try {
                $this->refreshToken();         
                $userinfoResponse = $this->fetchUserInfo();
            } catch (\Exception $e) {
                return $this->returnError($response, 'Get User Info Fehlgeschlagen:' . $e->getMessage());     
            }
        }
       
        $userInfo = json_decode($userinfoResponse->getBody());
        $user = $this->userFactory->create($userInfo);
        $userInfoPrettyFirst = json_encode( $userInfo, JSON_PRETTY_PRINT);

        $this->refreshToken(); 

        $userinfoResponseSecond = $this->fetchUserInfo();
    
        $userInfoSecond = json_decode($userinfoResponseSecond->getBody());
        $userInfoPrettySecond = json_encode($userInfoSecond, JSON_PRETTY_PRINT);
        
        return $this->view->render($response, 'info.html', [
            'user' => $user,
            'roles' => explode(',', $userInfo->Roles),
            'userInfoPrettyFirst' => $userInfoPrettyFirst,
            'userInfoPrettySecond' => $userInfoPrettySecond
        ]);
    }

    public function callback($request, $response, $args) {
        $parameters = $request->getQueryParams();    
        $state = $this->session->get('state', '');

        if($parameters["state"] !==  $state->check) {
            $this->session::destroy();
            return $this->returnError($response, 'Callback Fehlgeschlagen: State check missmatch!');
        }

        if ($state->state === 'logout') {
            return $this->logoutCallback($request, $response, $args);
        }

        if ($state->state === 'login') {
            return $this->loginCallback($request, $response, $args);
        }

        if ($state->state === 'checksession') {
            return $this->checkSessionCallback($request, $response, $args);
        }

        return $this->returnError($response, 'Callback Fehlgeschlagen: Unknown callback state "' . $state->state . '"');
    }

    
    
    public function loginCallback($request, $response, $args) {
        $parameters = $request->getQueryParams(); 
        $provider = $this->getCurrentProvider();
        $discovery = $provider->discovery();
        
        try {
            $tokenResponse = $this->http->request('POST', $discovery->token_endpoint , [
                'form_params' => [
                    'code' => $parameters["code"],
                    'client_id' => $provider->clientId(),
                    'client_secret' => $provider->clientSecret(),
                    'redirect_uri' => $provider->redirectUri(),
                    'grant_type' => 'authorization_code'
                ]
            ]);
        } catch (\Exception $e) {
            $this->session::destroy();
            return $this->returnError($response, 'Token Request Fehlgeschlagen: ' . $e->getMessage());    
        }

        $tokens = json_decode($tokenResponse->getBody());
        $idToken = (array) $this->jwtPayload($tokens->id_token);
        
        if($idToken['nonce'] !== $this->session->get('nonce', '')) {
            $this->session::destroy();
            return $this->returnError($response, 'ID Token Request Fehlgeschlagen: nonce missmatch');    
        }

        $this->session->set('tokens', $tokens);
        return $response->withStatus(200)->withRedirect('/info');     
    }

    public function checkSessionCallback($request, $response, $args) {
        $parameters = $request->getQueryParams(); 
        if (array_key_exists('error', $parameters)) {
            return $this->logoutCallback($request, $response, $args);
        }

        $provider = $this->getCurrentProvider();
        $discovery = $provider->discovery();   
        $tokens = $this->session->get('tokens'); 
        
        $state = (object) [
            'check' => bin2hex(openssl_random_pseudo_bytes(20)),
            'state' => 'logout' 
        ];
        $this->session->set('state', $state);

        $logoutParameters = [
            'id_token_hint' => $tokens->id_token,
            'post_logout_redirect_uri' => $provider->redirectUri(),
            'state' => $state->check
        ];
        $query = http_build_query($logoutParameters);
        return $response->withStatus(200)->withRedirect($discovery->end_session_endpoint . '?' . $query  );
    }

    public function logoutCallback($request, $response, $args) {
        $provider = $this->session->get('provider');
        $tokens = $this->session->get('tokens');
        try {
            $this->revokeToken($tokens->access_token);
            $this->revokeToken($tokens->refresh_token);
        } catch (\Exception $e){}
        $this->session::destroy();
        return $response->withStatus(200)->withRedirect('/login/' . $provider);  
    }

    private function fetchUserInfo() {
        $url = $this->getCurrentProvider()->discovery()->userinfo_endpoint;
        $tokens = $this->session->get('tokens'); 
        return $this->http->request('GET', $url , [
            'headers' => [
                'Authorization' => 'Bearer ' .  $tokens->access_token,
                'Accept'        => 'application/json' 
            ]
        ]);
    }

    private function revokeToken($token) {
        $provider = $this->getCurrentProvider();
        $discovery = $provider->discovery();
        $this->http->request('POST', $discovery->revocation_endpoint, [
            'auth' => [$provider->clientId(), $provider->clientSecret()],
            'form_params' => [
                'token' => $token
            ]
        ]);
    }

    private function refreshToken() {
        $provider = $this->getCurrentProvider();
        $tokens = $this->session->get('tokens');
        $tokenResponse = $this->http->request('POST', $provider->discovery()->token_endpoint , [
            'form_params' => [
                'client_id' => $provider->clientId(),
                'client_secret' => $provider->clientSecret(),
                'refresh_token' => $tokens->refresh_token,
                'grant_type' => 'refresh_token',
                'scope' > $provider->scope()
            ]
        ]);
        $tokens = json_decode($tokenResponse->getBody());
        $this->session->set('tokens', $tokens);
    }

    private function getCurrentProvider() {
        return $this->container->get("providers")[$this->session->get('provider')];    
    }

    private function returnError($response, $message) {
        return $this->view->render(
            $response->withStatus(400), 
            'error.html',
            ['errorMessage' => $message]
        );     
    }

    private function jwtPayload($jwt) {
        $tks = explode('.', $jwt);
        if (count($tks) != 3) {
            throw new UnexpectedValueException('Wrong number of segments');
        }
        list($headb64, $bodyb64, $cryptob64) = $tks;
        if (null === $payload = JWT::jsonDecode(JWT::urlsafeB64Decode($bodyb64))) {
            throw new UnexpectedValueException('Invalid payload encoding');
        }
        return $payload;
    }
    
}
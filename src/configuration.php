<?php

if(array_key_exists("SERVER_NAME",$_SERVER) && ( $_SERVER["SERVER_NAME"] == '127.0.0.1' || $_SERVER["SERVER_NAME"] == 'localhost')) {
    define ('ZS_SAC_OPENID_ENVIRONMENT', 'dev');
} else {
    define ('ZS_SAC_OPENID_ENVIRONMENT', 'prod');
}

define('ZS_SAC_SECTION_ID', 5800);

<?php

namespace Sketch;

class Provider
{
    public function __construct($configuration) {
        $this->configuration = (object) [
            'clientId' => $configuration['clientId'],
            'clientSecret' => $configuration['clientSecret'],
            'discovery' => (object) $configuration['discovery'],
            'scope' => 'openid'
        ];
    } 

    public function clientId() {
        return $this->configuration->clientId;
    }

    public function clientSecret() {
        return $this->configuration->clientSecret;
    }

    public function redirectUri() {
        if(ZS_SAC_OPENID_ENVIRONMENT === 'dev') {
            return 'http://localhost:8888/auth/callback';
        }        
        return 'https://anmeldung.sac-zindelspitz.ch/auth/callback';
    }

    public function discovery() {
        return $this->configuration->discovery;
    }

    public function scope() {
        return $this->configuration->scope;
    }

}
<?php

namespace Sketch;

class UserFactory
{
    public function __construct() {
    } 

    public function create($userInfo) {
        
        $user = [
            'guid' => $userInfo->sub,

            'memberId' => intval($userInfo->contact_number),
            
            'firstName' => $userInfo->vorname,
            'lastName' => $userInfo->familienname,

            'email' => $userInfo->email,
            
            'street' => $userInfo->strasse,
            'postal' => $userInfo->plz,
            'city' => $userInfo->ort,
            'country' => $userInfo->land,

            'phoneHome' => $userInfo->telefonp,
            'phoneMobile' => $userInfo->telefonmobil,
            'phoneWork' => $userInfo->telefong,

            'dateOfBirth' => strtotime($userInfo->geburtsdatum),

            'sectionIds' => []
        ];

        $roles = explode(',', $userInfo->Roles);
        foreach ($roles as $role) {
            $id = $this->matchOrNull('/NAV_MITGLIED_S(\d+)/', $role);
            if($id) {
                $user['sectionIds'][] = intval($id);     
            }     
        }

        $user['isMemberOfSection'] = in_array(ZS_SAC_SECTION_ID, $user['sectionIds']);
        $user['isMemberOfSac'] = !empty($user['sectionIds']); 

        return $user;
    }

    private function matchOrNull($pattern, $string) {
        $m = preg_match($pattern, $string, $matches);
        if( $m === false || $m === 0) {
            return null;
        }
        return $matches[1];
    }


}
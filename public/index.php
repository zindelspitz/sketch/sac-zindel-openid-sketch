<?php
require '../src/secrets.php';
require '../src/configuration.php';
require '../vendor/autoload.php';

$config = [];
$config['displayErrorDetails'] = true;

$app = new \Slim\App(['settings' => $config]);

$app->add(new \Slim\Middleware\Session([
    'name' => 'sketch-openid-connect',
    'autorefresh' => true,
    'lifetime' => '1 hour'
]));

$container = $app->getContainer();
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('../templates', [
        'cache' => false
    ]);
    $router = $container->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));
    return $view;
};
$container['session'] = function () {
    return new \SlimSession\Helper;
};
$container['providers'] = function () {
    return [
        'dev' => new \Sketch\Provider(ZS_SAC_OPENID_CONFIG['dev']),
        'prod' => new \Sketch\Provider(ZS_SAC_OPENID_CONFIG['prod'])
    ]; 
};

$container['userFactory'] = function () {
    return new \Sketch\UserFactory;
};

$app->get('/', \Sketch\OpenIdConnect::class . ':home');
$app->get('/login/prod', \Sketch\OpenIdConnect::class . ':loginProd');
$app->get('/login/dev', \Sketch\OpenIdConnect::class . ':loginDev');
$app->get('/auth/callback', \Sketch\OpenIdConnect::class . ':callback');
$app->get('/info', \Sketch\OpenIdConnect::class . ':info');
$app->get('/logout', \Sketch\OpenIdConnect::class . ':logout');

// Run app
$app->run();